<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
require_once 'Robot.php';
require_once 'FactoryRobot.php';
require_once 'MergeRobots.php';

$factory = new FactoryRobot();
$factory->addType('one', 1, 1, 1);
$factory->addType('two', 2, 2, 2);
try {
	$robotsOne = $factory->createRobot('one', 2);
	$robotsTwo = $factory->createRobot('two', 2);
}
catch (Exception $e){
	echo $e->getMessage();
}
echo '<pre>';
print_r($robotsOne);
print_r($robotsTwo);

$merge = new MergeRobots();
try {
	$merge->addRobot($factory->createRobot('one', 1)[0]);
	$merge->addRobot($factory->createRobot('two', 1)[0]);
}
catch (Exception $e) {
	echo $e->getMessage();
}
echo 'Min Velosity: ' . $merge->getSpeed();
echo '<br>';
echo 'Sum Weight: ' . $merge->getWeight();

