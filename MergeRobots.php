<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
class MergeRobots
{
    /**
     * @var array
     */
    private $robots = [];

    /**
     * @var string
     */
    private $totalType = [];

    /**
     * @var float
     */
    private $totalWeight = [];

    /**
     * @var float
     */
    private $totalHeight = [];

    /**
     * @var float
     */
    private $totalVelocity = [];

    /**
     * @param Robot $robot
     * @return Robot
     */
    function addRobot(Robot $robot)
    {
        $this->totalType[] = $robot->type;
        $this->totalWeight[] = $robot->weight;
        $this->totalHeight[] = $robot->height;
        $this->totalVelocity[] = $robot->velocity;
        return new Robot(implode($this->totalType), array_sum($this->totalWeight), array_sum($this->totalHeight), min($this->totalVelocity));
    }

    function getSpeed()
    {
        return min($this->totalVelocity);
    }

    function getWeight()
    {
        return array_sum($this->totalWeight);
    }


}
