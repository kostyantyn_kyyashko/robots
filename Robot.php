<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
class Robot
{
    public function __construct($type, $weight, $height, $velocity)
    {
        $this->type = $type;
        $this->weight = $weight;
        $this->height = $height;
        $this->velocity = $velocity;
        return $this;
    }

    public $weight;
    public $height;
    public $velocity;

}
