<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
class FactoryRobot
{
    /**
     * @var array
     */
    public $types = [];

    /**
     * @param $type string
     * @param $velosity float
     * @param $weight float
     * @param $height float
     */
    public function addType($type, $velosity, $weight, $height)
    {
        $this->types[$type] = [
            'velosity' => $velosity,
            'weight' => $weight,
            'height' => $height,
        ];
    }


    public function createRobot($type, $count)
    {
        if(!array_key_exists($type, $this->types)) {
            throw new Exception("This type $type not defined");
        }
        $out = [];
        for($i=0;$i<$count;$i++) {
            $out[] = new Robot(
                $type,
                $this->types[$type]['velosity'],
                $this->types[$type]['weight'],
                $this->types[$type]['height']
            );
        }
        return $out;
    }

}
